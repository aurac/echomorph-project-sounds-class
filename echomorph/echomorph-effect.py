import numpy as np
import scipy.signal as signal
import scipy.io.wavfile as wavfile

def highpass_filter(samples, fs):
    """
    Apply a highpass filter to the input samples using an IIR filter.

    Args:
        samples (ndarray): Input audio samples.
        fs (int): Sampling frequency of the audio.

    Returns:
        ndarray: Filtered audio samples.
    """
    # Design the highpass IIR filter
    cutoff_freq = 4000  # 4KHz
    nyquist = 0.5 * fs
    b, a = signal.iirfilter(4, cutoff_freq/nyquist, btype='high', ftype='butter')
    
    # Convert the filter coefficients to SOS format
    butter_sos = signal.iirfilter(4, cutoff_freq/nyquist, btype='high', ftype='butter', output='sos')
    
    # Apply the filter to the samples
    filtered_samples = signal.sosfilt(butter_sos, samples)
    
    return filtered_samples

def peak(samples):
    """
    Find the peak value in the input samples.

    Args:
        samples (ndarray): Input audio samples.

    Returns:
        float: Peak value.
    """
    return np.max(np.abs(samples))

def normalize(samples, fraction):
    """
    Normalize the input samples to a given fraction of the peak value.

    Args:
        samples (ndarray): Input audio samples.
        fraction (float): Fraction of the peak value to normalize to.

    Returns:
        ndarray: Normalized audio samples.
    """
    peak_value = peak(samples)
    normalized_samples = samples * (fraction / peak_value)
    return normalized_samples

def distort(samples):
    """
    Apply clipping distortion to the input samples.

    Args:
        samples (ndarray): Input audio samples.

    Returns:
        ndarray: Distorted audio samples.
    """
    normalized_samples = normalize(samples, 1.0)
    clipped_samples = np.clip(normalized_samples, -0.4, 0.4)
    return clipped_samples

def mix(n, samples):
    """
    Mix the input samples with another set of samples.

    Args:
        n (ndarray): Input audio samples to mix with.
        samples (ndarray): Input audio samples to be mixed.

    Returns:
        ndarray: Mixed audio samples.
    """
    mixed_samples = (0.8 * n) + (0.2 * samples)
    return mixed_samples

def main():
    # Read the WAV file
    sampling_rate, samples = wavfile.read("voice-note.wav")

    # Initialize the output with the input samples
    output = samples.copy()

    # Apply effects to the input samples
    for _ in range(9):
        p = peak(samples)
        d = distort(samples)
        h = highpass_filter(d, sampling_rate)
        n = normalize(h, p)
        m = mix(n, samples)  # Usar las muestras originales para mezclar
        samples = samples.astype(np.float64) * 0.7
        output = np.append(output, samples)

    # Write the output to a WAV file
    wavfile.write("output.wav", sampling_rate, output.astype(np.int16))

if __name__ == "__main__":
    main()

